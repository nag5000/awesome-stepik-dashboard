1. Add `.env` file:
```
YOUTRACK_AUTH=<your youtrack auth token>
GITHUB_AUTH=<your github auth token with repo scope>
YOUTRACK_AGILE_FRD_BOARD_ID=79-31
YOUTRACK_AGILE_BND_BOARD_ID=79-34
```
2. Start server: `npm install`, then `npm start` or `nodemon` (`npm install -g nodemon`).
3. Start client: `cd client && npm install && npm start`
