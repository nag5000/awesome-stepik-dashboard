const express = require('express');
const path = require('path');
const cors = require('cors');

require('dotenv').config();

const {
  initBoards,
  fetchDashboardData
} = require('./dashboard');

const app = express();

// Serve the static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));

app.use(cors());

app.get('/dashboard/:board?/:sprint?', async (req, res) => {
  try {
    let { board, sprint } = req.params;
    let data = await fetchDashboardData(board, sprint);
    res.json(data);
  } catch(e) {
    res.status(500).json({error: e.message});
  }
});

// Handles any requests that don't match the ones above
app.get('*', (req,res) => {
  res.sendFile(path.join(__dirname+'/client/build/index.html'));
});

initBoards().then(() => {
  const port = 3001;
  app.listen(port, () => console.log(`Listening on port ${port}`));
});
