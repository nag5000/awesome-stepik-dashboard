import React from 'react';

import api from './DashboardAPI';
import './Dashboard.css';

import CommandBar from './CommandBar';

// TODO: App should own and cache "dashboardData".
// https://www.robinwieruch.de/react-global-state-without-redux/#react-global-state-with-pages
class Dashboard extends React.Component {
  constructor(props) {
    super(props);

    const location = props.location;
    const searchParams = new URLSearchParams(location.search);

    const filterQuery = searchParams.get('filter') || null;

    this.state = {
      error: null,
      isLoaded: false,
      data: null,
      selectedSprintName: searchParams.get('sprint') || null,
      filterQuery,
      filter: this.deserializeFilterQuery(filterQuery)
    };

    this.handleSprintChange = this.handleSprintChange.bind(this);
    this.applyCommand = this.applyCommand.bind(this);
  }

  handleSprintChange(event) {
    let sprintName = event.target.value;
    this.changeSprint(sprintName);
  }

  changeSprint(sprintName) {
    const searchParams = new URLSearchParams(this.props.location.search);
    if (sprintName) {
      searchParams.set('sprint', sprintName);
    }

    const search = searchParams.toString();
    this.props.history.push(`?${search}`);
  }

  removeFilter(filterKey) {
    if (!(filterKey in this.state.filter)) {
      return;
    }

    const filter = {...this.state.filter};
    delete filter[filterKey];

    const searchParams = new URLSearchParams(this.props.location.search);
    const filterQuery = this.searilizeFilterQuery(filter);
    if (filterQuery) {
      searchParams.set('filter', filterQuery);
    } else {
      searchParams.delete('filter');
    }

    const search = searchParams.toString();
    this.props.history.push(`?${search}`);
  }

  updateFilter(filterKey, filterValue) {
    if (this.state.filter[filterKey] === filterValue) {
      return;
    }

    const filter = {...this.state.filter};
    filter[filterKey] = filterValue;

    const searchParams = new URLSearchParams(this.props.location.search);
    const filterQuery = this.searilizeFilterQuery(filter);
    searchParams.set('filter', filterQuery);

    const search = searchParams.toString();
    this.props.history.push(`?${search}`);
  }

  applyCommand(cmdKey, cmdValue, cmdDef) {
    if (cmdDef.type === 'filter') {
      if (cmdDef.defaultValue !== cmdValue) {
        this.updateFilter(cmdKey, cmdValue);
      } else {
        this.removeFilter(cmdKey);
      }
    } else if (cmdDef.type === 'command') {
      if (cmdKey === 'sprint') {
        const sprintName = cmdValue;
        this.changeSprint(sprintName);
      }
    }
  }

  searilizeFilterQuery(filterHash) {
    if (!filterHash) {
      return null;
    }

    let entries = Object.entries(filterHash);
    if (entries.length === 0) {
      return null;
    }

    return entries
      .map(([key, value]) => key + '=' + value)
      .join('&');
  }

  deserializeFilterQuery(filterQuery) {
    let hash = {};
    if (!filterQuery) {
      return hash;
    }

    filterQuery.split('&').forEach(expr => {
      let [key, value] = expr.split('=', 2);
      hash[key] = (value !== undefined) ? value : true;
    });

    return hash;
  }

  loadDashboard(sprintName) {
    this.setState({
      selectedSprintName: sprintName,
      isLoaded: false
    });

    api.fetchDashboardData(sprintName).then(data => {
      this.setState({
        isLoaded: true,
        data: data,
        selectedSprintName: data.sprintName,
        error: null
      });
    }).catch(e => {
      this.setState({
        isLoaded: true,
        error: e
      });
    });
  }

  componentDidMount() {
    let sprintName = this.state.selectedSprintName || '';
    this.loadDashboard(sprintName);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.location.search !== prevProps.location.search) {
      const searchParams = new URLSearchParams(this.props.location.search);
      const prevSearchParams = new URLSearchParams(prevProps.location.search);

      const filterQuery = searchParams.get('filter') || null;
      const prevFilterQuery = prevSearchParams.get('filter') || null;
      if (filterQuery !== prevFilterQuery) {
        this.setState({
          filterQuery: filterQuery,
          filter: this.deserializeFilterQuery(filterQuery)
        });
      }

      const sprintName = searchParams.get('sprint') || '';
      const prevSprintName = prevSearchParams.get('sprint') || '';
      if (sprintName !== prevSprintName) {
        this.loadDashboard(sprintName);
      }
    }
  }

  render() {
    const state = this.state;

    const dashboardHeader = (
      <div className="dashboard__header">
        <CommandBar
          sprintName={state.selectedSprintName}
          filter={state.filter}
          dashboardData={state.data}
          applyCommand={this.applyCommand}
        />
      </div>
    );

    if (state.error) {
      return (
        <main className="dashboard">
          <div className="toast toast-error">
            <h6>Error</h6>
            {state.error.message}
          </div>
        </main>
      );
    } else if (!state.isLoaded) {
      return (
        <main className="dashboard">
          {dashboardHeader}
          <div className="empty">
            <div className="loading loading-lg"></div>
            <p className="empty-title h5">
              Loading ...
            </p>
          </div>
        </main>
      );
    } else {
      let taskList;
      let tasks = Object.values(state.data.tasks);

      if (tasks.length) {
        let filteredTasks = tasks;

        let priorityFilter = state.filter.priority;
        if (priorityFilter) {
          filteredTasks = filteredTasks.filter(
            t => t.fields.priority.name === priorityFilter
          );
        }

        let stateFilter = state.filter.state;
        if (stateFilter) {
          filteredTasks = filteredTasks.filter(
            t => t.fields.state.name === stateFilter
          );
        }

        let assigneeFilter = state.filter.assignee;
        if (assigneeFilter) {
          let assignee = (assigneeFilter !== 'Unassigned')
              ? assigneeFilter
              : null;

          filteredTasks = filteredTasks.filter(
            t => (t.fields.assignee && t.fields.assignee.name) === assignee
          );
        }

        let tagFilter = state.filter.tag;
        if (tagFilter) {
          let tagFilters = tagFilter.split(',').map(t => t.trim());
          filteredTasks = filteredTasks.filter(task => {
            let taskTagNames = task.tags.map(t => t.name);
            return tagFilters.every(t => taskTagNames.includes(t));
          });
        }

        let boardFilter = state.filter.board;
        if (boardFilter) {
          filteredTasks = filteredTasks.filter(
            t => t.boardName === boardFilter
          );
        }

        let prFilter = state.filter.pullRequest;
        if (prFilter) {
          let filterFunc;
          switch(prFilter) {
            case 'yes':
              filterFunc = function(task) {
                return task.pullRequests.length > 0;
              };
              break;

            case 'no':
              filterFunc = function(task) {
                return task.pullRequests.length === 0;
              };
              break;

            case 'open':
              filterFunc = function(task) {
                return task.pullRequests.some(pr => pr.state === 'OPEN');
              };
              break;

            case 'closed':
              filterFunc = function(task) {
                return task.pullRequests.some(pr => pr.state === 'CLOSED');
              };
              break;

            case 'merged':
              filterFunc = function(task) {
                return task.pullRequests.some(pr => pr.state === 'MERGED');
              };
              break;

            default:
              throw new Error('Invalid "pullRequest" filter: ' + prFilter);
          }

          filteredTasks = filteredTasks.filter(filterFunc);
        }

        let textFilter = state.filter.text;
        if (textFilter) {
          filteredTasks = filteredTasks.filter(
            t => t.summary.includes(textFilter)
          );
        }

        let items = filteredTasks.map(
          task => <YoutrackTaskItem
                    task={task}
                    key={task.id}
                  />
        );

        taskList = <ul className="dashboard__task-list">{items}</ul>;
      } else {
        taskList = (
          <div className="empty">
            <p className="empty-title h5">
              There are no tasks in sprint "{state.data.sprint}".
            </p>
          </div>
        );
      }

      let prList;
      let otherPullRequests = Object.values(state.data.otherPullRequests);

      if (otherPullRequests.length) {
        let items = otherPullRequests.map(
          pr => <PullRequestItem
                  pullRequest={pr}
                  key={pr.number}
                />
        );

        prList = (
          <>
            <div
              className="divider text-center dashboard__lists-divider"
              data-content="Other Pull Requests from the Milestone"
            ></div>
            <ul className="dashboard__other-pr-list">{items}</ul>
          </>
        );
      }

      return (
        <main className="dashboard">
          {dashboardHeader}
          {taskList}
          {prList}
        </main>
      );
    }
  }
}

function PullRequestItem(props) {
  const pr = props.pullRequest;
  return (
    <li className="pr-item">
      <a
        href={pr.url}
        target="_blank"
        rel="noopener noreferrer"
        className="pr-item__number"
      >
        #{pr.number}
      </a>
      <span className="pr-item__title">{pr.title}</span>
      <span className="pr-item__task-list">
        {pr.youtrackIssues.map(issue => (
          <a
            href={'https://vyahhi.myjetbrains.com/youtrack/issue/'
                  + issue.id}
            target="_blank"
            rel="noopener noreferrer"
            className="pr-item__task-link label label-secondary"
            key={issue.id}
          >
            {issue.id}
          </a>
        ))}
      </span>
    </li>
  );
}

function YoutrackTaskItem(props) {
  const task = props.task;

  const pulls = task.pullRequests.map(pr => {
    let stateCssClass;
    if (pr.state === 'OPEN') {
      stateCssClass = 'label-success';
    } else if (pr.state === 'CLOSED') {
      stateCssClass = 'label-error';
    } else if (pr.state === 'MERGED') {
      stateCssClass = 'label-primary';
    } else {
      stateCssClass = '';
    }

    return (
      <span
        key={pr.number}
        className="task__pr"
      >
        <a
          className={'task__pr-number label ' + stateCssClass}
          href={pr.url}
          target="_blank"
          rel="noopener noreferrer"
          title={`[${pr.state}] ${pr.title}`}
        >#{pr.number}</a>
      </span>
    );
  });

  return (
    <li
      className="task"
      data-priority={task.fields.priority.name}
      data-state={task.fields.state.name}
      data-type={task.fields.type.name}
    >
      <div className="task__row task-fields">
        <span
          className="task__priority task-field"
          data-priority={task.fields.priority.name}
        ></span>

        <a
          href={'https://vyahhi.myjetbrains.com/youtrack/issue/'
                + task.id}
          target="_blank"
          rel="noopener noreferrer"
          className="task__id task-field"
        >
          {task.id}
        </a>

        <span
          className="task__state label label-rounded task-field"
          data-state={task.fields.state.name}
        >
          {task.fields.state.name}
        </span>

        { /* TODO: a.href to youtrack board? */ }
        <span
          className="task__board label task-field"
          title={`Board: ${task.boardName}`}
          data-board-name={task.boardName}
        ></span>

        <div className="task__assignee chip task-field">
          <figure
            className="avatar avatar-sm"
            data-initial={
              task.fields.assignee
                ? task.fields.assignee.name.split(' ').map(x => x[0]).join('')
                : '?'
            }
          >
            {task.fields.assignee
             && task.fields.assignee.avatarUrl
             && (
               <img
                 src={'https://vyahhi.myjetbrains.com' + task.fields.assignee.avatarUrl}
                 alt={`${task.fields.assignee.name}'s avatar`}
               />
             )}
          </figure>
          <span className="task__assignee-name">
            {task.fields.assignee ? task.fields.assignee.name : 'Unassigned'}
          </span>
        </div>

        {pulls.length > 0 &&
         <div className="task__pr-list task-field">
           {pulls}
         </div>
        }

        {task.tags.length > 0 &&
         <div className="task__tags task-field">
           {task.tags.map(t => (
             <span
               key={t.name}
               className="task__tag label"
               style={{
                 color: t.color.foreground,
                 backgroundColor: t.color.background
               }}
             >{t.name}</span>
           ))}
         </div>
        }
      </div>

      <div className="task__row">
        <span className="task__title">{task.summary}</span>
      </div>
    </li>
  );
}

export default Dashboard;
