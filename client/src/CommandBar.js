import React from 'react';

import './CommandBar.css';

const CommandDefinitions = {
  sprint: {
    type: 'command',
    inputRegex: /Sprint: (?<value>R[0-9]+)$/,
    getAutocompleteItems(input, dashboardData) {
      if (input) {
        return Object.keys(dashboardData.boardData.sprints)
          .reverse()
          .map(s => `Sprint: ${s}`)
          .filter(x => x.startsWith(input))
          .slice(0, input.length > 'Sprint'.length ? 15 : 6);
      } else {
        return ['Sprint: '];
      }
    }
  },

  priority: {
    type: 'filter',
    defaultValue: undefined,
    chipTemplate: 'Priority: {value}',
    inputRegex: /Priority: (?<value>[a-zA-Z-]+)$/,
    getAutocompleteItems(input, dashboardData) {
      if (input) {
        return [
          'Priority: Minor',
          'Priority: Normal',
          'Priority: Major',
          'Priority: Critical',
          'Priority: Show-stopper'
        ].filter(x => x.startsWith(input));
      } else {
        return ['Priority: '];
      }
    }
  },

  state: {
    type: 'filter',
    defaultValue: undefined,
    chipTemplate: 'State: {value}',
    inputRegex: /State: (?<value>[a-zA-Z ]+)$/,
    getAutocompleteItems(input, dashboardData) {
      if (input) {
        return [
          'State: In Progress',
          'State: Verified',
          'State: Needs Testing',
          'State: Reopened'
        ].filter(x => x.startsWith(input));
      } else {
        return ['State: '];
      }
    }
  },

  assignee: {
    type: 'filter',
    defaultValue: undefined,
    chipTemplate: 'Assignee: {value}',
    inputRegex: /Assignee: (?<value>[a-zA-Z0-9 ]+)$/,
    getAutocompleteItems(input, dashboardData) {
      if (input) {
        let assigneeList = Object.values(dashboardData.tasks)
            .map(t => t.fields.assignee && t.fields.assignee.name)
            .filter(Boolean)
            .concat('Unassigned');

        // Remove duplicates.
        assigneeList = [...new Set(assigneeList)];

        return assigneeList
          .map(s => `Assignee: ${s}`)
          .filter(x => x.startsWith(input))
          .slice(0, input.length > 'Assignee'.length ? 15 : 6);
      } else {
        return ['Assignee: '];
      }
    }
  },

  tag: {
    type: 'filter',
    defaultValue: undefined,
    chipTemplate: 'Tag: {value}',
    inputRegex: /Tag: (?<value>[a-zA-Z, _-]+)$/,
    getAutocompleteItems(input, dashboardData) {
      if (input) {
        let arr = [
          'Tag: frontend_sprint_backlog',
          'Tag: backend_sprint_backlog',
          'Tag: backlog',
          'Tag: pre_backlog'
        ];

        // Support free-form input.
        // It is possible to enter multiple tags, separated by comma.
        if (this.inputRegex.test(input)) {
          arr = arr.concat(input);
        }

        // Remove duplicates.
        arr = [...new Set(arr)];

        return arr.filter(x => x.startsWith(input));
      } else {
        return ['Tag: '];
      }
    }
  },

  board: {
    type: 'filter',
    defaultValue: undefined,
    chipTemplate: 'Board: {value}',
    inputRegex: /Board: (?<value>[a-zA-Z -_]+)$/,
    getAutocompleteItems(input, dashboardData) {
      if (input) {
        return Object.values(dashboardData.boardData.boards)
          .map(b => `Board: ${b.name}`)
          .filter(x => x.startsWith(input));
      } else {
        return ['Board: '];
      }
    }
  },

  pullRequest: {
    type: 'filter',
    defaultValue: undefined,
    chipTemplate: 'PR: {value}',
    inputRegex: /PR: (?<value>(yes|no|open|closed|merged))$/,
    getAutocompleteItems(input, dashboardData) {
      if (input) {
        return [
          'PR: yes',
          'PR: no',
          'PR: open',
          'PR: merged',
          'PR: closed'
        ].filter(x => x.startsWith(input));
      } else {
        return ['PR: '];
      }
    }
  },

  text: {
    type: 'filter',
    defaultValue: undefined,
    chipTemplate: 'Search: {value}',
    inputRegex: /Search: "(?<value>.*)"$/,
    getAutocompleteItems(input, dashboardData) {
      if (input) {
        return this.inputRegex.test(input)
          ? [input]
          : [`Search: "${input}"`];
      } else {
        return null;
      }
    }
  }
};

class CommandBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      menuIsOpened: false,
      autocompleteItems: undefined
    };

    this.commandInput = React.createRef();
    this.commandBarContainer = React.createRef();

    this.onInputFocus = this.onInputFocus.bind(this);
    this.onFilterClear = this.onFilterClear.bind(this);
    this.handleInputKeyPress = this.handleInputKeyPress.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.autocompleteItem = this.autocompleteItem.bind(this);
    this._onDocumentClick = this._onDocumentClick.bind(this);
  }

  onInputFocus() {
    if (!this.state.autocompleteItems) {
      const value = this.commandInput.current.value;
      this.updateAutocompleteItems(value);
    }

    this.openMenu();
  }

  openMenu() {
    this.setState({
      menuIsOpened: true
    });

    document.addEventListener('click', this._onDocumentClick, true);
  }

  closeMenu() {
    this.setState({
      menuIsOpened: false
    });

    document.removeEventListener('click', this._onDocumentClick, true);
  }

  _onDocumentClick(e) {
    if (this.state.menuIsOpened
        && !this.commandBarContainer.current.contains(e.target)) {
      this.closeMenu();
    }
  }

  onFilterClear(filterKey) {
    let def = CommandDefinitions[filterKey];
    this.props.applyCommand(filterKey, def.defaultValue, def);
    this.commandInput.current.focus();
  }

  handleInputKeyPress(e) {
    if (e.keyCode === 13 /* Enter */) {
      let value = e.target.value;
      value = value && value.trim();

      if (!value) {
        return;
      }

      const [cmdKey, cmdValue, cmdDef] = this.parseCommand(value);
      this.props.applyCommand(cmdKey, cmdValue, cmdDef);

      const inputEl = this.commandInput.current;
      inputEl.value = '';
      this.updateAutocompleteItems('');

      return;
    }

    if (e.keyCode === 27 /* Escape */) {
      this.closeMenu();
      return;
    }

    if (e.keyCode === 8 /* Backspace */) {
      let value = e.target.value;
      if (!value) {
        // If input is empty, remove last applied filter.
        let filters = Object.keys(this.props.filter);
        if (filters.length > 0) {
          let lastFilter = filters[filters.length - 1];
          this.onFilterClear(lastFilter);
        }
      }

      return;
    }
  }

  handleInputChange(e) {
    // TODO: throttle
    let value = e.target.value;
    this.updateAutocompleteItems(value);
  }

  updateAutocompleteItems(inputValue) {
    const autocompleteItems = [];
    const dashboardData = this.props.dashboardData;

    for (let key in CommandDefinitions) {
      const def = CommandDefinitions[key];
      const items = def.getAutocompleteItems(inputValue, dashboardData);
      if (items) {
        autocompleteItems.push(...items);
      }
    }

    this.setState({
      autocompleteItems
    });
  }

  autocompleteItem(value, event) {
    const inputEl = this.commandInput.current;
    inputEl.value = value;
    inputEl.focus();

    this.updateAutocompleteItems(value);

    event.preventDefault();
  }

  parseCommand(inputValue) {
    for (let key in CommandDefinitions) {
      const def = CommandDefinitions[key];
      let match = def.inputRegex && def.inputRegex.exec(inputValue);
      if (match) {
        return [key, match.groups.value, def];
      }
    }

    return ['text', inputValue, CommandDefinitions.text];
  }

  render() {
    const state = this.state;

    const filterElements = [];

    const filter = this.props.filter || {};
    for (let filterKey in filter) {
      let def = CommandDefinitions[filterKey];
      if (def && def.type === 'filter') {
        let value = filter[filterKey];
        filterElements.push(
          <span
            key={filterKey}
            className="chip"
            data-filter-key={filterKey}
            data-filter-value={value}
          >
            {def.chipTemplate.replace('{value}', value)}
            <button
              className="btn btn-clear"
              onClick={() => this.onFilterClear(filterKey)}
              aria-label="Close"
            ></button>
          </span>
        );
      }
    }

    return (
      <div
        ref={this.commandBarContainer}
        className="command-bar form-autocomplete"
      >
        <div
          className={
            'form-autocomplete-input form-input '
              + (state.menuIsOpened ? 'is-focused' : '')
          }
        >
          <span className="chip active">
            Sprint: {this.props.sprintName || '...'}
          </span>

          {filterElements}

          {/* <span className="chip"> */}
          {/*   Bruce Banner */}
          {/*   <a className="btn btn-clear" */}
          {/*      href="#" */}
          {/*      aria-label="Close" */}
          {/*      role="button"></a> */}
          {/* </span> */}

          {/* <div className="chip"> */}
          {/*   <img className="avatar avatar-sm" */}
          {/*        src="../img/avatar-1.png" */}
          {/*        alt="Avatar"/> */}
          {/*   Thor Odinson */}
          {/* </div> */}

          {/* <span className="chip"> */}
          {/*   Natasha Romanoff */}
          {/* </span> */}

          <input
            ref={this.commandInput}
            className="form-input"
            type="text"
            placeholder="Command, Filter, Sort..."
            onKeyDown={this.handleInputKeyPress}
            onInput={this.handleInputChange}
            onFocus={this.onInputFocus}
          />
        </div>

        <ul
          className="menu"
          hidden={!state.menuIsOpened
                  || !state.autocompleteItems
                  || !state.autocompleteItems.length}
        >
          {state.autocompleteItems && state.autocompleteItems.map(item => (
            <li
              className="menu-item"
              key={item}
            >
              <a
                href="#autocomplete"
                role="button"
                onClick={e => this.autocompleteItem(item, e)}
              >
                {item}
              </a>
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default CommandBar;
