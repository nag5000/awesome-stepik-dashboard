export default {
  async fetchDashboardData(sprintName) {
    let url = `/dashboard/${sprintName}`;
    const res = await fetch(url);
    const json = await res.json();

    if (!res.ok) {
      throw new Error(`[${res.status}] ${json.error}`);
    }

    return json;
  }
};
