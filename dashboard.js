const fetch = require('node-fetch');

const dashboardData = {};

async function initBoards() {
  const sprints = {};
  const boards = {};
  let currentSprintName;

  const boardIds = [
    process.env.YOUTRACK_AGILE_FRD_BOARD_ID,
    process.env.YOUTRACK_AGILE_BND_BOARD_ID
  ];

  for (let boardId of boardIds) {
    let board = await fetchBoard(boardId);

    currentSprintName = currentSprintName
      || normalizeSprintName(board.currentSprint.name);

    board.sprints.forEach(s => {
      let name = normalizeSprintName(s.name);
      if (!name) {
        return;
      }

      let sprint = (sprints[name] || (sprints[name] = {
        name,
        sources: [],
        milestone: null
      }));
      sprint.sources.push({
        boardId: boardId,
        sprintId: s.id
      });
    });

    boards[boardId] = {
      id: boardId,
      name: board.name
    };
  }

  const milestones = await fetchMilestones();
  milestones.forEach(milestone => {
    let sprint = sprints[milestone.title];
    if (sprint) {
      sprint.milestone = milestone;
    }
  });

  if (!currentSprintName) {
    throw new Error('currentSprintName is not defined');
  }

  dashboardData.currentSprintName = currentSprintName;
  dashboardData.sprints = sprints;
  dashboardData.boards = boards;
}

// Frontend sprint R135 -> R135
// Returns null for old sprint names.
function normalizeSprintName(sprintName) {
  let match = sprintName.match(/R[0-9]+$/);
  let normalized = match && match[0];
  return normalized;
}

async function fetchBoard(boardId) {
  let url = `https://vyahhi.myjetbrains.com/youtrack/api/agiles/${boardId}`;
  url += '?fields=id,name,sprints(id,archived,isDefault,name),currentSprint(id,name)';

  const res = await fetch(url, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + process.env.YOUTRACK_AUTH
    }
  });

  const board = await res.json();
  return board;
}

async function fetchDashboardData(sprintName) {
  const sprint = sprintName
        ? dashboardData.sprints[sprintName]
        : dashboardData.sprints[dashboardData.currentSprintName];

  if (!sprint) {
    throw new Error('Sprint not found');
  }

  const data = {
    // Dashboard summary data.
    boardData: dashboardData,

    // Selected sprint name.
    sprintName: sprint.name,

    // Tasks related to selected sprint.
    // Dictionary <task.idReadable, hash>
    tasks: {},

    // The rest of the Pull Requests from the milestone, not mapped to
    // any Youtrack tasks from the sprint.
    // Dictionary <pullRequest.number, hash>
    otherPullRequests: {}
  };

  let issuesPromises = sprint.sources.map(
    s => fetchSprintIssues(s.boardId, s.sprintId)
  );

  const [pullRequests, ...youtrackIssuesArrays] = await Promise.all([
    sprint.milestone ? fetchPullRequests(sprint.milestone.number) : [],
    ...issuesPromises
  ]);

  // Flatten array of arrays.
  const youtrackIssues = youtrackIssuesArrays
        .reduce((acc, val) => acc.concat(val), []);

  youtrackIssues.forEach(issue => {
    if (issue.idReadable in data.tasks) {
      return;
    }

    let fieldsDict = {};
    if (issue.fields) {
      issue.fields.forEach(f => {
        try {
          let key = f.projectCustomField.field.name.toLowerCase();
          fieldsDict[key] = f.value || null;
        } catch(ok) {
          // It's ok.
        }
      });
    }

    let tags = [];
    if (issue.tags) {
      tags = issue.tags.map(t => ({
        name: t.name,
        color: {
          background: t.color.background,
          foreground: t.color.foreground
        }
      }));
    }

    let task = {
      id: issue.idReadable,
      summary: issue.summary,
      fields: fieldsDict,
      tags: tags,
      boardId: issue.boardId,
      boardName: dashboardData.boards[issue.boardId].name,
      pullRequests: []
    };

    data.tasks[issue.idReadable] = task;
  });

  pullRequests.forEach(pr => {
    let prHash = {
      number: pr.number,
      url: pr.url,
      state: pr.state,
      title: pr.title
    };

    let text = pr.body;
    let textTasksMatch = text.match(/\*\*Задача\*\*:.*#EDY-[0-9]+/);
    textTasksMatch = textTasksMatch && textTasksMatch[0];

    let prTaskStringIds = textTasksMatch
        && textTasksMatch.match(/#EDY-[0-9]+/g);

    if (prTaskStringIds) {
      // Remove "#" sign.
      prTaskStringIds.map(x => x.substr(1)).forEach(taskIdReadable => {
        let task = data.tasks[taskIdReadable];
        let otherPullRequest = data.otherPullRequests[prHash.number];

        if (task) {
          task.pullRequests.push(prHash);
        } else if (otherPullRequest) {
          otherPullRequest.youtrackIssues.push({
            id: taskIdReadable
          });
        } else {
          data.otherPullRequests[prHash.number] = {
            ...prHash,
            youtrackIssues: [{
              id: taskIdReadable
            }]
          };
        }
      });
    } else {
      data.otherPullRequests[prHash.number] = {
        ...prHash,
        youtrackIssues: []
      };
    }
  });

  return data;
}

async function fetchMilestones() {
  let url = 'https://api.github.com/graphql';
  let query = `query {
    repository(owner: "bioinf", name: "edy") {
      milestones(last: 100) {
        edges {
          node {
            id
            title
            state
            number
            url
          }
        }
      }
    }
  }`;

  const res = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'bearer ' + process.env.GITHUB_AUTH
    },
    body: JSON.stringify({query: query})
  });

  let json = await res.json();

  if (res.ok) {
    let milestones = json.data.repository.milestones;
    json = milestones.edges.map(edge => edge.node);
  } else {
    let err = new Error('Request error');
    err.status = res.status;
    err.response = json;
    throw err;
  }

  return json;
}

async function fetchPullRequests(milestone) {
  let url = 'https://api.github.com/graphql';

  let query = `query {
    repository(owner: "bioinf", name: "edy") {
      milestone(number: ${milestone}) {
        pullRequests(first: 100) {
          edges {
            node {
              id
              title
              state
              number
              url
              body
            }
          }
        }
      }
    }
  }`;

  const res = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'bearer ' + process.env.GITHUB_AUTH
    },
    body: JSON.stringify({query: query})
  });

  let json = await res.json();

  if (res.ok) {
    let milestone = json.data.repository.milestone;
    if (milestone) {
      json = milestone.pullRequests.edges.map(edge => edge.node);
    } else {
      // Requested milestone does not exist.
      json = [];
    }
  } else {
    let err = new Error('Request error');
    err.status = res.status;
    err.response = json;
    throw err;
  }

  return json;
}

async function fetchIssueVcsChanges(issueId) {
  let url = `https://vyahhi.myjetbrains.com/youtrack/api/issues/${issueId}/vcsChanges`;
  url += '?fields=$type,agile(id),attachments($type,author(fullName,id,ringId),comment(id),created,id,imageDimension(height,width),issue(id,project(id,ringId)),mimeType,name,removed,size,thumbnailURL,url,visibility($type,implicitPermittedUsers($type,avatarUrl,email,fullName,id,issueRelatedGroup(icon),login,name,online,ringId),permittedGroups($type,allUsersGroup,icon,id,name,ringId),permittedUsers($type,avatarUrl,email,fullName,id,issueRelatedGroup(icon),login,name,online,ringId))),author($type,avatarUrl,email,fullName,id,issueRelatedGroup(icon),login,name,online,ringId),author($type,avatarUrl,email,fullName,id,issueRelatedGroup(icon),login,name,online,ringId),branch,color(id),commands(end,errorText,hasError,start),comment(id),created,created,creator($type,avatarUrl,email,fullName,id,issueRelatedGroup(icon),login,name,online,ringId),date,date,deleted,duration(id,minutes,presentation),fetched,files,id,id,id,id,id,id,id,idReadable,isDraft,localizedName,mimeType,minutes,name,name,noHubUserReason(id),noUserReason(id),numberInProject,presentation,processor($type,committers,enabled,handle,handle,id,login,params,progress(message),project(id),repoName,repoOwnerName,repository,repositoryOwner,server(id,url,enabled),stateMessage,tcId,upsourceHubResourceKey,upsourceProjectName,version),processors($type,committers,enabled,handle,handle,id,login,params,progress(message),project(id),repoName,repoOwnerName,repository,repositoryOwner,server(id,url,enabled),stateMessage,tcId,upsourceHubResourceKey,upsourceProjectName,version),project($type,id,name,plugins(timeTrackingSettings(enabled,estimate(field(id,name),id),timeSpent(field(id,name),id)),vcsIntegrationSettings(processors(enabled,url,upsourceHubResourceKey,server(enabled,url)))),ringId,shortName),removed,resolved,shortName,size,state,summary,text,text,text,text,textPreview,textPreview,thumbnailURL,type(id,name),url,urls,user($type,avatarUrl,email,fullName,id,issueRelatedGroup(icon),login,name,online,ringId),userName,usesMarkdown,usesMarkdown,version,visibility($type,implicitPermittedUsers($type,avatarUrl,email,fullName,id,issueRelatedGroup(icon),login,name,online,ringId),permittedGroups($type,allUsersGroup,icon,id,name,ringId),permittedUsers($type,avatarUrl,email,fullName,id,issueRelatedGroup(icon),login,name,online,ringId))';

  const res = await fetch(url, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + process.env.YOUTRACK_AUTH
    }
  });

  const json = await res.json();
  return json;
}

/**
 * API: https://www.jetbrains.com/help/youtrack/standalone/api-agiles-id-sprints-id-issues.html
 * Supported fields: https://www.jetbrains.com/help/youtrack/standalone/api-issues-id.html#supported-fields
 */
async function fetchSprintIssues(boardId, sprintId) {
  let url = 'https://vyahhi.myjetbrains.com/youtrack/api/agiles'
      + `/${boardId}/sprints/${sprintId}/issues`
      + '?fields=id,idReadable,summary,fields(projectCustomField(field(name)),value(name,avatarUrl)),tags(name,color(background,foreground))';

  const res = await fetch(url, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + process.env.YOUTRACK_AUTH
    }
  });

  const json = await res.json();

  json.forEach(issue => {
    issue.boardId = boardId;
  });

  return json;
}

async function fetchSprintActivities(sprintName) {
  let url = 'https://vyahhi.myjetbrains.com/youtrack/api/activitiesPage';
  url += `?query=Planned for: ${sprintName}`;
  url += '&categories=VcsChangeCategory';
  url += '&fields=activities($type,added($type,$type,$type,agile(id),attachments($type,author(fullName,id,ringId),comment(id),created,id,imageDimension(height,width),issue(id,project(id,ringId)),mimeType,name,removed,size,thumbnailURL,url,visibility($type,implicitPermittedUsers($type,avatarUrl,email,fullName,id,issueRelatedGroup(icon),login,name,online,ringId),permittedGroups($type,allUsersGroup,icon,id,name,ringId),permittedUsers($type,avatarUrl,email,fullName,id,issueRelatedGroup(icon),login,name,online,ringId))),author($type,avatarUrl,email,fullName,id,issueRelatedGroup(icon),login,name,online,ringId),author($type,avatarUrl,email,fullName,id,issueRelatedGroup(icon),login,name,online,ringId),branch,color(id),commands(end,errorText,hasError,start),comment(id),created,created,creator($type,avatarUrl,email,fullName,id,issueRelatedGroup(icon),login,name,online,ringId),date,date,deleted,duration(id,minutes,presentation),fetched,files,id,id,id,id,id,id,id,idReadable,isDraft,localizedName,mimeType,minutes,name,name,noHubUserReason(id),noUserReason(id),numberInProject,presentation,processor($type,committers,enabled,handle,handle,id,login,params,progress(message),project(id),repoName,repoOwnerName,repository,repositoryOwner,server(id,url,enabled),stateMessage,tcId,upsourceHubResourceKey,upsourceProjectName,version),processors($type,committers,enabled,handle,handle,id,login,params,progress(message),project(id),repoName,repoOwnerName,repository,repositoryOwner,server(id,url,enabled),stateMessage,tcId,upsourceHubResourceKey,upsourceProjectName,version),project($type,id,name,plugins(timeTrackingSettings(enabled,estimate(field(id,name),id),timeSpent(field(id,name),id)),vcsIntegrationSettings(processors(enabled,url,upsourceHubResourceKey,server(enabled,url)))),ringId,shortName),removed,resolved,shortName,size,state,summary,text,text,text,text,textPreview,textPreview,thumbnailURL,type(id,name),url,urls,user($type,avatarUrl,email,fullName,id,issueRelatedGroup(icon),login,name,online,ringId),userName,usesMarkdown,usesMarkdown,version,visibility($type,implicitPermittedUsers($type,avatarUrl,email,fullName,id,issueRelatedGroup(icon),login,name,online,ringId),permittedGroups($type,allUsersGroup,icon,id,name,ringId),permittedUsers($type,avatarUrl,email,fullName,id,issueRelatedGroup(icon),login,name,online,ringId))),author($type,avatarUrl,email,fullName,id,issueRelatedGroup(icon),login,name,online,ringId),authorGroup(icon,name),category(id),field($type,customField(id,fieldType(isMultiValue,valueType)),id,linkId,presentation),id,markup,removed($type,$type,agile(id),color(id),id,id,idReadable,isDraft,localizedName,name,numberInProject,project($type,id,name,plugins(timeTrackingSettings(enabled,estimate(field(id,name),id),timeSpent(field(id,name),id)),vcsIntegrationSettings(processors(enabled,url,upsourceHubResourceKey,server(enabled,url)))),ringId,shortName),resolved,summary,text),target(created,id,usesMarkdown),targetMember,targetSubMember,timestamp)';

  const res = await fetch(url, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + process.env.YOUTRACK_AUTH
    }
  });

  const json = await res.json();
  return json && json.activities;
}

module.exports = {
  initBoards,
  fetchDashboardData
};
